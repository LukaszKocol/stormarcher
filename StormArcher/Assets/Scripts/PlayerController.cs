﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController instance;

    public float moveSpeed;
    public Rigidbody2D theRB;

    private Animator anim;

    public SpriteRenderer theSR;
    public Sprite[] playerDirectionSprite;

    public Animator wpnAnim;

    private bool isKnockingBack;
    public float knockbackTime, knockbackForce;
    private float knockbackCounter;
    private Vector2 knockDir;

    public GameObject hitEffect;

    public float dashSpeed, dashLenght, dashStaminaCost;
    private float dashCounter, activeMoveSpeed;

    public float totalStamina, staminaRefillSpeed;
    public float currentStamina;

    private bool isSpinning;

    public float spinCost, spinColldown;

    private float spinCounter;
    public bool canMove;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        theRB = GetComponent<Rigidbody2D>();
        activeMoveSpeed = moveSpeed;
        currentStamina = totalStamina;

        UIManager.instance.UpdateStamina();
    }

    // Update is called once per frame
    void Update()
    {
        if (!canMove || GameManager.instance.dialogActive)
        {
            theRB.velocity = Vector2.zero;
            anim.SetFloat("Speed", 0f);
            return;
        }

        if (!isKnockingBack)
        {
            theRB.velocity = new Vector2((Input.GetAxisRaw("Horizontal")), (Input.GetAxisRaw("Vertical"))).normalized * activeMoveSpeed;

            anim.SetFloat("Speed", theRB.velocity.magnitude);

            if (theRB.velocity != Vector2.zero)
            {
                if (Input.GetAxisRaw("Horizontal") != 0)
                {
                    theSR.sprite = playerDirectionSprite[1];

                    if (Input.GetAxisRaw("Horizontal") < 0)
                    {
                        wpnAnim.SetFloat("dirX", -1f);
                        wpnAnim.SetFloat("dirY", 0f);
                        theSR.flipX = true;
                    }
                    else
                    {
                        theSR.flipX = false;
                        wpnAnim.SetFloat("dirX", 1f);
                        wpnAnim.SetFloat("dirY", 0f);
                    }
                }

                if (Input.GetAxisRaw("Vertical") != 0)
                {
                    if (Input.GetAxisRaw("Vertical") < 0)
                    {
                        wpnAnim.SetFloat("dirX", 0f);
                        wpnAnim.SetFloat("dirY", -1f);
                        theSR.sprite = playerDirectionSprite[2];
                    }
                    else
                    {
                        theSR.sprite = playerDirectionSprite[0];
                        wpnAnim.SetFloat("dirX", 0f);
                        wpnAnim.SetFloat("dirY", 1f);
                    }
                }
            }

            if (Input.GetMouseButtonDown(0) && !isSpinning)
            {
                AudioManager.instance.PlaySFX(0);
                wpnAnim.SetTrigger("Attack");
            }

            if (dashCounter <= 0)
            {
                if (Input.GetKeyDown(KeyCode.Space) && currentStamina >= dashStaminaCost)
                {
                    activeMoveSpeed = dashSpeed;
                    dashCounter = dashLenght;
                    currentStamina -= dashStaminaCost;
                }
            }
            else
            {
                dashCounter -= Time.deltaTime;
                if (dashCounter <= 0)
                {
                    activeMoveSpeed = moveSpeed;
                }
            }

            if (spinCounter <= 0)
            {
                if (Input.GetMouseButtonDown(1) && currentStamina >= spinCost)
                {
                    wpnAnim.SetTrigger("RoundAttack");
                    currentStamina -= spinCost;
                    spinCounter = spinColldown;
                    isSpinning = true;
                    AudioManager.instance.PlaySFX(0);

                }
            }
            else
            {
                spinCounter -= Time.deltaTime;

                if (spinCounter <= 0)
                {
                    isSpinning = false;
                }
            }

            currentStamina += staminaRefillSpeed * Time.deltaTime;
            if (currentStamina > totalStamina)
            {
                currentStamina = totalStamina;
            }

            UIManager.instance.UpdateStamina();
        }
        else
        {
            knockbackCounter -= Time.deltaTime;
            theRB.velocity = knockDir * knockbackForce;

            if (knockbackCounter <= 0)
            {
                isKnockingBack = false;
            }
        }
    }

    public void KnockBack(Vector3 knockerPosition)
    {
        knockbackCounter = knockbackTime;
        isKnockingBack = true;

        knockDir = transform.position - knockerPosition;

        Instantiate(hitEffect, transform.position, transform.rotation);
    }

    public void DoAtLevelStart()
    {
        canMove = true;
    }
}
