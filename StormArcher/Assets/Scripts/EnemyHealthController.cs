﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthController : MonoBehaviour
{
    public int currentHealth;
    public GameObject deathEffect;
    public EnemyController theEC;

    public GameObject healthToDrop;
    public float healthDropChance;
    public GameObject cointToDrop;
    public float cointToDropChance;
    // Start is called before the first frame update
    void Start()
    {
        theEC = GetComponent<EnemyController>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void TakeDamage(int damageAmount)
    {
        theEC.KnockBack(PlayerController.instance.transform.position);
        currentHealth -= damageAmount;

        if (currentHealth <= 0)
        {
            if (deathEffect != null)
            {
                Instantiate(deathEffect, transform.position, transform.rotation);
            }

            Destroy(gameObject);

            if (Random.Range(0f, 100f) < healthDropChance && healthToDrop != null)
            {
                Instantiate(healthToDrop, transform.position, transform.rotation);
            }

            if (Random.Range(0f, 100f) < cointToDropChance && cointToDrop != null)
            {
                Instantiate(cointToDrop, transform.position, transform.rotation);
            }

        }
        AudioManager.instance.PlaySFX(7);

    }
}
