﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour
{
    public GameObject objectToswitchOff;

    public bool inRange;
    private bool isOn;
    public SpriteRenderer SR;
    public Sprite spriteOn, spriteOff;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (inRange)
        {
            if (Input.GetMouseButtonDown(0))
            {
                isOn = !isOn;
                objectToswitchOff.SetActive(!isOn);

                if (isOn)
                {
                    SR.sprite = spriteOn;
                }
                else
                {
                    SR.sprite = spriteOff;
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            inRange = true;
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            inRange = false;
        }
    }
}
