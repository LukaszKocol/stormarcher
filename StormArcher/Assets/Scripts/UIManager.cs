﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;
    public Slider healthSlider;
    public TMP_Text healthText;
    public Slider staminaSlider;
    public TMP_Text staminaText;
    public Slider bossHealth;
    public TMP_Text bossName;

    public TMP_Text coinText;

    public string mainMenuScene;

    public GameObject pauseScreen;

    public GameObject blackoutScreen;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        UpdateHealth();
        UpdateStamina();
        UpdateCoins();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateHealth()
    {
        healthSlider.maxValue = PlayerHealthController.instance.maxHealth;
        healthSlider.value = PlayerHealthController.instance.currentHealth;
        healthText.text = "HEALTH " + PlayerHealthController.instance.currentHealth + "/" + PlayerHealthController.instance.maxHealth;
    }

    public void UpdateStamina()
    {
        staminaSlider.maxValue = PlayerController.instance.totalStamina;
        staminaSlider.value = PlayerController.instance.currentStamina;
        staminaText.text = "STAMINA " + Mathf.RoundToInt(PlayerController.instance.currentStamina) + "/" + PlayerController.instance.totalStamina;
    }

    public void UpdateCoins()
    {
        coinText.text = "COINS: " + GameManager.instance.currentCoins;
    }

    public void MaineMenu()
    {
        SceneManager.LoadScene(mainMenuScene);
    }

    public void QuitGame()
    {
        Debug.Log("Quitting game");
        Application.Quit();
    }

    public void ResumeGame()
    {
        GameManager.instance.PauseUnpause();
    }

    public void ActivateBossHealthBar()
    {
        bossHealth.gameObject.SetActive(true);
        bossName.gameObject.SetActive(true);
    }

    public void DeactivateBossHealthBar()
    {
        bossHealth.gameObject.SetActive(false);
        bossName.gameObject.SetActive(false);
    }
}
