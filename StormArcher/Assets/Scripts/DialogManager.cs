﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DialogManager : MonoBehaviour
{
    public static DialogManager instance;
    public GameObject dialogPanel;
    public TMP_Text dialogText;
    public int currentLine;

    public string[] dialogTexts;

    private bool justStarted;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.dialogActive)
        {
            if (Input.GetMouseButtonUp(0))
            {
                if (!justStarted)
                {
                    currentLine++;

                    if (currentLine >= dialogTexts.Length)
                    {
                        dialogPanel.SetActive(false);
                        GameManager.instance.dialogActive = false;
                    }
                    else
                    {
                        dialogText.text = dialogTexts[currentLine];
                    }
                }
                else
                {
                    justStarted = false;
                }
            }
        }
    }

    public void ShowDialog(string[] newLines)
    {
        dialogTexts = newLines;
        currentLine = 0;
        dialogText.text = dialogTexts[currentLine];
        dialogPanel.SetActive(true);

        justStarted = true;

        GameManager.instance.dialogActive = true;
    }
}
