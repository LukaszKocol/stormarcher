﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopItem : MonoBehaviour
{
    [TextArea]
    public string description;
    public int itemCost;
    private bool itemActive;

    public bool isHealthUp, isStaminaUp;
    public int amountToAdd;

    public bool removeAfterPurchuase;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (itemActive)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (GameManager.instance.currentCoins >= itemCost)
                {
                    GameManager.instance.currentCoins -= itemCost;
                    UIManager.instance.UpdateCoins();

                    if (isHealthUp)
                    {
                        PlayerHealthController.instance.maxHealth += amountToAdd;
                        PlayerHealthController.instance.currentHealth += amountToAdd;

                        UIManager.instance.UpdateHealth();
                    }

                    if (isStaminaUp)
                    {
                        PlayerController.instance.totalStamina += amountToAdd;
                        PlayerController.instance.currentStamina += amountToAdd;

                        UIManager.instance.UpdateStamina();
                    }

                    if (removeAfterPurchuase)
                    {
                        gameObject.SetActive(false);
                    }

                    DialogManager.instance.dialogPanel.SetActive(false);
                }
                else
                {
                    DialogManager.instance.dialogText.text = "You don`t have enough coins";
                }
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            itemActive = true;

            DialogManager.instance.dialogPanel.SetActive(true);
            DialogManager.instance.dialogText.text = description;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            itemActive = false;

            DialogManager.instance.dialogPanel.SetActive(false);
        }
    }
}
