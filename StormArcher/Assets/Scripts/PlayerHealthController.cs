﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealthController : MonoBehaviour
{
    public static PlayerHealthController instance;

    public int currentHealth;
    public int maxHealth;

    public float invencibilityLenght = 1f;
    private float invincibilityCounter;

    public GameObject deathEffect;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        UIManager.instance.UpdateHealth();
    }

    // Update is called once per frame
    void Update()
    {
        if (invincibilityCounter > 0)
        {
            invincibilityCounter -= Time.deltaTime;
        }
    }

    public void DamagePlayer(int damage)
    {
        if (invincibilityCounter <= 0)
        {
            currentHealth -= damage;

            invincibilityCounter = invencibilityLenght;

            if(currentHealth <= 0)
            {
                currentHealth = 0;
                gameObject.SetActive(false);
                AudioManager.instance.PlaySFX(4);

                Instantiate(deathEffect, transform.position, transform.rotation);
                Instantiate(deathEffect, transform.position, transform.rotation);
                Instantiate(deathEffect, transform.position, transform.rotation);
            }
        }

        UIManager.instance.UpdateHealth();
        AudioManager.instance.PlaySFX(7);
    }

    public void RestoreHealth(int healthToRestore)
    {
        currentHealth += healthToRestore;

        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }

        UIManager.instance.UpdateHealth();
    }
}
