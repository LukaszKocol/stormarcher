﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController instance;
    private Camera theCam;
    private Transform target;
    private float halfWidth, halfHeight;
    public BoxCollider2D areaBox;
    // Start is called before the first frame update

    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        theCam = GetComponent<Camera>();
        target = PlayerController.instance.transform;
        halfHeight = theCam.orthographicSize;
        halfWidth = theCam.orthographicSize * theCam.aspect;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = new Vector3(target.position.x, target.position.y, transform.position.z);

        if (areaBox != null)
        {
            transform.position = new Vector3(Mathf.Clamp(target.position.x, areaBox.bounds.min.x + halfWidth, areaBox.bounds.max.x - halfWidth)
                , Mathf.Clamp(target.position.y, areaBox.bounds.min.y + halfHeight, areaBox.bounds.max.y - halfHeight), transform.position.z);
        }
    }
}
