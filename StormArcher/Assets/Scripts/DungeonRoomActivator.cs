﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonRoomActivator : MonoBehaviour
{
    public GameObject[] allEnemies;
    public List<GameObject> clonedEnemies = new List<GameObject>();

    public GameObject[] doors;

    public bool lockDoors;
    private bool doorslocked;
    private bool dontSpawnEnemies = false;

    public bool isBossRoom;
    public Transform bossCamLower, bossCamUpper;
    public GameObject theBoss;
    private bool dontReactivateTheBoss;
    // Start is called before the first frame update
    void Start()
    {
        foreach (GameObject enemy in allEnemies)
        {
            enemy.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (doorslocked)
        {
            bool enemyFound = false;

            for (int i = 0; i < clonedEnemies.Count; i++)
            {
                if (clonedEnemies[i] != null)
                {
                    enemyFound = true;
                }
            }

            if (!enemyFound)
            {
                ChangeDoorsState(false);
                lockDoors = false;
                doorslocked = false;
                dontSpawnEnemies = true;
            }
        }

    }

    private void SpawnEnemies()
    {
        if (!dontSpawnEnemies)
        {
            foreach (GameObject enemy in allEnemies)
            {
                GameObject newEnemy = Instantiate(enemy, enemy.transform.position, enemy.transform.rotation);
                newEnemy.SetActive(true);
                clonedEnemies.Add(newEnemy);
            }
        }
    }

    private void DespawnEnemies()
    {
        foreach (GameObject enemy in clonedEnemies)
        {
            Destroy(enemy);
        }

        clonedEnemies.Clear();
    }

    public void ChangeDoorsState(bool state)
    {
        foreach (GameObject door in doors)
        {
            door.SetActive(state);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            if (lockDoors)
            {
                ChangeDoorsState(true);
                doorslocked = true;
            }

            SpawnEnemies();
            DungeonCameraController.instance.targetPoint = new Vector3(transform.position.x, transform.position.y, DungeonCameraController.instance.targetPoint.z);

            if (isBossRoom)
            {
                
                DungeonCameraController.instance.ActivateBossRoom(bossCamUpper.position, bossCamLower.position);

                if (!dontReactivateTheBoss)
                {
                    theBoss.SetActive(true);
                    dontReactivateTheBoss = true;
                }
            }
            else
            {
                DungeonCameraController.instance.inBossRoom = false;
            }
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            if (PlayerHealthController.instance.currentHealth > 0)
            {
                DespawnEnemies();
            }
        }
    }
}
