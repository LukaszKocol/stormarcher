﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickup : MonoBehaviour
{
    public int healthToRestore;
    public float lifeTime = 10;

    public float waitToPickup = .5f;
    // Start is called before the first frame update
    void Start()
    {
        if (lifeTime > 0)
        {
            Destroy(gameObject, lifeTime);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (waitToPickup > 0)
        {
            waitToPickup -= Time.deltaTime;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player" && waitToPickup <= 0)
        {
            AudioManager.instance.PlaySFX(6);
            PlayerHealthController.instance.RestoreHealth(healthToRestore);
            Destroy(gameObject);
        }
    }
}
