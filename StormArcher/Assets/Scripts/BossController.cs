﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour
{
    public string bossName;
    public int bossHealth;
    public int stage2Treshold;
    public int stage3Treshold;

    public GameObject deathEffect;
    public GameObject theBoss, door;

    public Transform[] spawnPoints;
    private Vector3 moveTarget;
    public float moveSpeed;

    public float timeActive, timeBetweenSpawns, firstSpawnDelay;
    private float activeCounter, spawnCounter;

    public BossShot theShot;
    public Transform[] shotPoints;
    public Transform shotCenter;
    public float timeBetweenShots, shotRotateSpeed, shootTime;
    private float shotCounter, shootingCounter;
    public AudioSource levelBGM, bossBGM;
    // Start is called before the first frame update
    void Start()
    {
        door.SetActive(true);

        spawnCounter = firstSpawnDelay;

        UIManager.instance.bossHealth.maxValue = bossHealth;
        UIManager.instance.bossHealth.value = bossHealth;
        UIManager.instance.bossName.text = bossName;

        UIManager.instance.ActivateBossHealthBar();
        levelBGM.Stop();
        bossBGM.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (bossHealth <= 0)
            return;

        if (spawnCounter > 0)
        {
            spawnCounter -= Time.deltaTime;

            if (spawnCounter < 0)
            {
                activeCounter = timeActive;
                shootingCounter = shootTime;

                theBoss.transform.position = spawnPoints[Random.Range(0, spawnPoints.Length)].position;

                moveTarget = spawnPoints[Random.Range(0, spawnPoints.Length)].position;

                while (moveTarget == theBoss.transform.position)
                {
                    moveTarget = spawnPoints[Random.Range(0, spawnPoints.Length)].position;
                }

                theBoss.SetActive(true);
            }
        }
        else
        {
            activeCounter -= Time.deltaTime;
            if (activeCounter <= 0)
            {
                spawnCounter = timeBetweenSpawns;
                theBoss.SetActive(false);
            }

            theBoss.transform.position = Vector3.MoveTowards(theBoss.transform.position, moveTarget, moveSpeed * Time.deltaTime);

            if (shootingCounter > 0)
            {
                shootingCounter -= Time.deltaTime;
                shotCounter -= Time.deltaTime;
                if (shotCounter <= 0)
                {
                    shotCounter = timeBetweenShots;

                    if (bossHealth > stage2Treshold)
                    {

                        for (int i = 0; i < 4; i++)
                        {
                            Instantiate(theShot, shotPoints[i].position, shotPoints[i].rotation).SetDirection(shotCenter.position);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < shotPoints.Length; i++)
                        {
                            Instantiate(theShot, shotPoints[i].position, shotPoints[i].rotation).SetDirection(shotCenter.position);
                        }
                    }
                }
            }

            if (bossHealth < stage3Treshold)
            {
                shotCenter.transform.rotation = Quaternion.Euler(
                    shotCenter.transform.rotation.eulerAngles.x,
                    shotCenter.transform.rotation.eulerAngles.y,
                    shotCenter.transform.rotation.eulerAngles.z + (shotRotateSpeed * Time.deltaTime));
            }
        }
    }

    public void TakeDamage(int damageToTake)
    {
        bossHealth -= damageToTake;

        if (bossHealth <= 0)
        {
            bossHealth = 0;

            levelBGM.Play();
            bossBGM.Stop();
            theBoss.SetActive(false);
            door.SetActive(false);
            UIManager.instance.DeactivateBossHealthBar();
            Instantiate(deathEffect, theBoss.transform.position, transform.rotation);
        }
        else
        {
            UIManager.instance.bossHealth.value = bossHealth;
        }
    }
}
